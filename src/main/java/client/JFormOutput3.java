
package client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per jFormOutput3 complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="jFormOutput3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="content" type="{http://web.formunix}ArrayOf_xsd_base64Binary"/>
 *         &lt;element name="tipoCopia" type="{http://web.formunix}ArrayOf_xsd_string"/>
 *         &lt;element name="returnCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="metadati" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "jFormOutput3", propOrder = {
    "content",
    "tipoCopia",
    "returnCode",
    "error",
    "metadati"
})
public class JFormOutput3 {

    @XmlElement(required = true, nillable = true)
    protected ArrayOfXsdBase64Binary content;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfXsdString tipoCopia;
    protected int returnCode;
    @XmlElement(required = true, nillable = true)
    protected String error;
    @XmlElement(required = true, nillable = true)
    protected String metadati;

    /**
     * Recupera il valore della proprietà content.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfXsdBase64Binary }
     *     
     */
    public ArrayOfXsdBase64Binary getContent() {
        return content;
    }

    /**
     * Imposta il valore della proprietà content.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfXsdBase64Binary }
     *     
     */
    public void setContent(ArrayOfXsdBase64Binary value) {
        this.content = value;
    }

    /**
     * Recupera il valore della proprietà tipoCopia.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfXsdString }
     *     
     */
    public ArrayOfXsdString getTipoCopia() {
        return tipoCopia;
    }

    /**
     * Imposta il valore della proprietà tipoCopia.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfXsdString }
     *     
     */
    public void setTipoCopia(ArrayOfXsdString value) {
        this.tipoCopia = value;
    }

    /**
     * Recupera il valore della proprietà returnCode.
     * 
     */
    public int getReturnCode() {
        return returnCode;
    }

    /**
     * Imposta il valore della proprietà returnCode.
     * 
     */
    public void setReturnCode(int value) {
        this.returnCode = value;
    }

    /**
     * Recupera il valore della proprietà error.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getError() {
        return error;
    }

    /**
     * Imposta il valore della proprietà error.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setError(String value) {
        this.error = value;
    }

    /**
     * Recupera il valore della proprietà metadati.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetadati() {
        return metadati;
    }

    /**
     * Imposta il valore della proprietà metadati.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetadati(String value) {
        this.metadati = value;
    }

}
