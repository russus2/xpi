
package client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPdf2Return" type="{http://web.formunix}jFormOutput2"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPdf2Return"
})
@XmlRootElement(name = "getPdf2Response")
public class GetPdf2Response {

    @XmlElement(required = true)
    protected JFormOutput2 getPdf2Return;

    /**
     * Recupera il valore della proprietà getPdf2Return.
     * 
     * @return
     *     possible object is
     *     {@link JFormOutput2 }
     *     
     */
    public JFormOutput2 getGetPdf2Return() {
        return getPdf2Return;
    }

    /**
     * Imposta il valore della proprietà getPdf2Return.
     * 
     * @param value
     *     allowed object is
     *     {@link JFormOutput2 }
     *     
     */
    public void setGetPdf2Return(JFormOutput2 value) {
        this.getPdf2Return = value;
    }

}
