package client;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Form {
    public static void main(String[] argv) throws MalformedURLException {
        disableSslVerification();
        URL endpoint = new URL("https://dev.studioform.it/api/jform-wrapper/jform/Form/services/FormWebService?wsdl");
        FormWebServiceService service = new FormWebServiceService(endpoint);
        FormWebService servicePort = service.getFormWebService();
        BindingProvider prov = (BindingProvider) servicePort;
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJPOGdjOWdBTTRLM2dHeGNCQ3JBeThXY1U1NXYwR0RqWnpRU0htM05vcXVVIn0.eyJleHAiOjE2NDY3NTE2ODMsImlhdCI6MTY0NjcyMjg4MywianRpIjoiZjY0MjVkMWMtZTk2NC00NmI3LWJkMDctZWVjOWM4MWFiYjNkIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5kZXYuc3R1ZGlvZm9ybS5ob3N0L2F1dGgvcmVhbG1zL1N0dWRpb2Zvcm0iLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiODkxNTIwNDYtNTZlZC00ZTM3LTg4NGUtZGE2ZDExODM1ZjBkIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoid3JpdGVyQkUiLCJzZXNzaW9uX3N0YXRlIjoiZGExMDA4YTUtNDYwYi00YzhkLWI0MDItMTM0ZGIyYzE1OWM2IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJkZWZhdWx0LXJvbGVzLXN0dWRpb2Zvcm0iLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCIsInNpZCI6ImRhMTAwOGE1LTQ2MGItNGM4ZC1iNDAyLTEzNGRiMmMxNTljNiIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6IkFOVE9OSU8gUlVTU08iLCJncm91cHMiOlsiL0Npc2NvVW1icmVsbGFHcm91cDAxIiwiL0RldmVsb3BlcnMiXSwicHJlZmVycmVkX3VzZXJuYW1lIjoicmEwMjQiLCJnaXZlbl9uYW1lIjoiQU5UT05JTyIsImZhbWlseV9uYW1lIjoiUlVTU08iLCJlbWFpbCI6ImFudG9uaW8ucnVzc29Ac3R1ZGlvZm9ybS5uZXQifQ.g52BbiPLwYZ0AVUNLR8NJs9od-WxTgh4YEj99sS5NvHmXdu0cGcIc1smreiLffVxFVWa_Y8cvSzckt4cRyUokBLCBMYn0vZgi_8BPuQekjiWwyZ0gU4duhwkJu1Bx84009flcMrV6tFMseG-r54eOnfq3WaYQ-TEmAfmRm86gzup8Jy_Url78BzhbmolcVj3S0bFWdrIf27wPK0Y_P4nt89LaPouuFtiAIo1Ht_rD5HC0_goQ7N7UNIYSwYsRTilFWqufAt0zOutTBkkx1eCEFI_2p0zxYmFaBphdD5i4TN7sYwGWCXPVEWxiIFNSznBXbbjs94RpQfi80f4M1Q45A";
        Map<String, List<String>> headers = new HashMap<>();
        headers.put("Authorization", Collections.singletonList("Bearer "+token));
        prov.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);

        String xmldata = "<form_job><form_section><form name=\"RUSSUS\" modVersion=\"0\" action=\"0\" formato=\"PDF\"><subform_group/></form></form_section></form_job>";
        byte[] pdf = servicePort.getPdf(xmldata);
        System.out.println(new String(pdf));
    }

    private static void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }
}
