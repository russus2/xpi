
package client;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPdf }
     * 
     */
    public GetPdf createGetPdf() {
        return new GetPdf();
    }

    /**
     * Create an instance of {@link GetPdf3 }
     * 
     */
    public GetPdf3 createGetPdf3() {
        return new GetPdf3();
    }

    /**
     * Create an instance of {@link InsertEmptyPDFResponse }
     * 
     */
    public InsertEmptyPDFResponse createInsertEmptyPDFResponse() {
        return new InsertEmptyPDFResponse();
    }

    /**
     * Create an instance of {@link GetPdf2 }
     * 
     */
    public GetPdf2 createGetPdf2() {
        return new GetPdf2();
    }

    /**
     * Create an instance of {@link GetPdf2Response }
     * 
     */
    public GetPdf2Response createGetPdf2Response() {
        return new GetPdf2Response();
    }

    /**
     * Create an instance of {@link JFormOutput2 }
     * 
     */
    public JFormOutput2 createJFormOutput2() {
        return new JFormOutput2();
    }

    /**
     * Create an instance of {@link InsertPDFWithSignBOX }
     * 
     */
    public InsertPDFWithSignBOX createInsertPDFWithSignBOX() {
        return new InsertPDFWithSignBOX();
    }

    /**
     * Create an instance of {@link GetPdfResponse }
     * 
     */
    public GetPdfResponse createGetPdfResponse() {
        return new GetPdfResponse();
    }

    /**
     * Create an instance of {@link InsertEmptyPDF }
     * 
     */
    public InsertEmptyPDF createInsertEmptyPDF() {
        return new InsertEmptyPDF();
    }

    /**
     * Create an instance of {@link DoPdfAndSendToENSoft }
     * 
     */
    public DoPdfAndSendToENSoft createDoPdfAndSendToENSoft() {
        return new DoPdfAndSendToENSoft();
    }

    /**
     * Create an instance of {@link InsertPDFWithSignBOXResponse }
     * 
     */
    public InsertPDFWithSignBOXResponse createInsertPDFWithSignBOXResponse() {
        return new InsertPDFWithSignBOXResponse();
    }

    /**
     * Create an instance of {@link DoPdfAndSendToENSoftResponse }
     * 
     */
    public DoPdfAndSendToENSoftResponse createDoPdfAndSendToENSoftResponse() {
        return new DoPdfAndSendToENSoftResponse();
    }

    /**
     * Create an instance of {@link GetPdf3Response }
     * 
     */
    public GetPdf3Response createGetPdf3Response() {
        return new GetPdf3Response();
    }

    /**
     * Create an instance of {@link JFormOutput3 }
     * 
     */
    public JFormOutput3 createJFormOutput3() {
        return new JFormOutput3();
    }

    /**
     * Create an instance of {@link ArrayOfXsdString }
     * 
     */
    public ArrayOfXsdString createArrayOfXsdString() {
        return new ArrayOfXsdString();
    }

    /**
     * Create an instance of {@link ArrayOfXsdBase64Binary }
     * 
     */
    public ArrayOfXsdBase64Binary createArrayOfXsdBase64Binary() {
        return new ArrayOfXsdBase64Binary();
    }

}
