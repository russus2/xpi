
package client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPdfReturn" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPdfReturn"
})
@XmlRootElement(name = "getPdfResponse")
public class GetPdfResponse {

    @XmlElement(required = true)
    protected byte[] getPdfReturn;

    /**
     * Recupera il valore della proprietà getPdfReturn.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getGetPdfReturn() {
        return getPdfReturn;
    }

    /**
     * Imposta il valore della proprietà getPdfReturn.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setGetPdfReturn(byte[] value) {
        this.getPdfReturn = value;
    }

}
