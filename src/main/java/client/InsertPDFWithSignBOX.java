
package client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="processID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="moduleProcessID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XML" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="error_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "processID",
    "moduleProcessID",
    "xml",
    "errorCode"
})
@XmlRootElement(name = "insertPDFWithSignBOX")
public class InsertPDFWithSignBOX {

    @XmlElement(required = true)
    protected String processID;
    @XmlElement(required = true)
    protected String moduleProcessID;
    @XmlElement(name = "XML", required = true)
    protected String xml;
    @XmlElement(name = "error_code", required = true)
    protected String errorCode;

    /**
     * Recupera il valore della proprietà processID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessID() {
        return processID;
    }

    /**
     * Imposta il valore della proprietà processID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessID(String value) {
        this.processID = value;
    }

    /**
     * Recupera il valore della proprietà moduleProcessID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModuleProcessID() {
        return moduleProcessID;
    }

    /**
     * Imposta il valore della proprietà moduleProcessID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModuleProcessID(String value) {
        this.moduleProcessID = value;
    }

    /**
     * Recupera il valore della proprietà xml.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXML() {
        return xml;
    }

    /**
     * Imposta il valore della proprietà xml.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXML(String value) {
        this.xml = value;
    }

    /**
     * Recupera il valore della proprietà errorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Imposta il valore della proprietà errorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

}
