
package client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPdf3Return" type="{http://web.formunix}jFormOutput3"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPdf3Return"
})
@XmlRootElement(name = "getPdf3Response")
public class GetPdf3Response {

    @XmlElement(required = true)
    protected JFormOutput3 getPdf3Return;

    /**
     * Recupera il valore della proprietà getPdf3Return.
     * 
     * @return
     *     possible object is
     *     {@link JFormOutput3 }
     *     
     */
    public JFormOutput3 getGetPdf3Return() {
        return getPdf3Return;
    }

    /**
     * Imposta il valore della proprietà getPdf3Return.
     * 
     * @param value
     *     allowed object is
     *     {@link JFormOutput3 }
     *     
     */
    public void setGetPdf3Return(JFormOutput3 value) {
        this.getPdf3Return = value;
    }

}
