
package client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="doPdfAndSendToENSoftReturn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "doPdfAndSendToENSoftReturn"
})
@XmlRootElement(name = "doPdfAndSendToENSoftResponse")
public class DoPdfAndSendToENSoftResponse {

    @XmlElement(required = true)
    protected String doPdfAndSendToENSoftReturn;

    /**
     * Recupera il valore della proprietà doPdfAndSendToENSoftReturn.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDoPdfAndSendToENSoftReturn() {
        return doPdfAndSendToENSoftReturn;
    }

    /**
     * Imposta il valore della proprietà doPdfAndSendToENSoftReturn.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDoPdfAndSendToENSoftReturn(String value) {
        this.doPdfAndSendToENSoftReturn = value;
    }

}
