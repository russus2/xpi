
package client;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.7-b01
 * Generated source version: 2.2
 *
 */
@WebServiceClient(name = "FormWebService", targetNamespace = "http://web.formunix", wsdlLocation = "http://jform.studioform.host:9082/Form/services/FormWebService?WSDL")
public class FormWebServiceService
    extends Service
{

    private final static URL FORMWEBSERVICESERVICE_WSDL_LOCATION;
    private final static WebServiceException FORMWEBSERVICESERVICE_EXCEPTION;
    private final static QName FORMWEBSERVICESERVICE_QNAME = new QName("http://web.formunix", "FormWebServiceService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://jform.studioform.host:9082/Form/services/FormWebService?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        FORMWEBSERVICESERVICE_WSDL_LOCATION = url;
        FORMWEBSERVICESERVICE_EXCEPTION = e;
    }

    public FormWebServiceService() {
        super(__getWsdlLocation(), FORMWEBSERVICESERVICE_QNAME);
    }

    public FormWebServiceService(WebServiceFeature... features) {
        super(__getWsdlLocation(), FORMWEBSERVICESERVICE_QNAME, features);
    }

    public FormWebServiceService(URL wsdlLocation) {
        super(wsdlLocation, FORMWEBSERVICESERVICE_QNAME);
    }

    public FormWebServiceService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, FORMWEBSERVICESERVICE_QNAME, features);
    }

    public FormWebServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public FormWebServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns FormWebService
     */
    @WebEndpoint(name = "FormWebService")
    public FormWebService getFormWebService() {
        return super.getPort(new QName("http://web.formunix", "FormWebService"), FormWebService.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns FormWebService
     */
    @WebEndpoint(name = "FormWebService")
    public FormWebService getFormWebService(WebServiceFeature... features) {
        return super.getPort(new QName("http://web.formunix", "FormWebService"), FormWebService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (FORMWEBSERVICESERVICE_EXCEPTION!= null) {
            throw FORMWEBSERVICESERVICE_EXCEPTION;
        }
        return FORMWEBSERVICESERVICE_WSDL_LOCATION;
    }

}
