
package client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="insertPDFWithSignBOXReturn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertPDFWithSignBOXReturn"
})
@XmlRootElement(name = "insertPDFWithSignBOXResponse")
public class InsertPDFWithSignBOXResponse {

    @XmlElement(required = true)
    protected String insertPDFWithSignBOXReturn;

    /**
     * Recupera il valore della proprietà insertPDFWithSignBOXReturn.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsertPDFWithSignBOXReturn() {
        return insertPDFWithSignBOXReturn;
    }

    /**
     * Imposta il valore della proprietà insertPDFWithSignBOXReturn.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsertPDFWithSignBOXReturn(String value) {
        this.insertPDFWithSignBOXReturn = value;
    }

}
